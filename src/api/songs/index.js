const SongsHandler = require('./handler');
const routes = require('./route');

module.exports = {
  name: 'Songs',
  version: '1.0.0',
  description: 'Songs API',
  register: async (server, { service, validator }) => {
    const songHandler = new SongsHandler(service, validator);
    server.route(routes(songHandler));
  },
};
