class SongsHandler {
  constructor(service, validator) {
    this.songService = service;
    this.validationService = validator;
    this.getSongs = this.getSongs.bind(this);
    this.getSongById = this.getSongById.bind(this);
    this.postSong = this.postSong.bind(this);
    this.putSong = this.putSong.bind(this);
    this.deleteSong = this.deleteSong.bind(this);
  }

  async getSongs(req, res) {
    try {
      const result = await this.songService.getSongs();
      return res.response({
        status: 'success',
        data: { songs: result.rows ?? [] },
      }).code(200);
    } catch (error) {
      return error;
    }
  }

  async getSongById(req, res) {
    const { id } = req.params;
    try {
      const result = await this.songService.getSongById(id);
      return res.response({
        status: 'success',
        data: { song: result },
      }).code(200);
    } catch (error) {
      return error;
    }
  }

  async postSong(req, res) {
    const data = req.payload;
    try {
      this.validationService.validation(data);
      const result = await this.songService.addSong(data);
      return res.response({
        status: 'success',
        data: {
          songId: result,
        },
      }).code(201);
    } catch (error) {
      return error;
    }
  }

  async putSong(req, res) {
    const { id } = req.params;
    const data = req.payload;
    try {
      this.validationService.validation(data);
      await this.songService.updateSong(id, data);
      return res.response({
        status: 'success',
        message: 'Song updated!',
      }).code(200);
    } catch (error) {
      return error;
    }
  }

  async deleteSong(req, res) {
    const { id } = req.params;
    try {
      await this.songService.deleteSong(id);
      return res.response({
        status: 'success',
        message: 'Song deleted!',
      }).code(200);
    } catch (error) {
      return error;
    }
  }
}

module.exports = SongsHandler;
