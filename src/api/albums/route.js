const routes = (handler) => [
  {
    method: 'GET',
    path: '/albums',
    handler: handler.getAlbums,
  },
  {
    method: 'GET',
    path: '/albums/{id}',
    handler: handler.getAlbumById,
  },
  {
    method: 'POST',
    path: '/albums',
    handler: handler.postAlbum,
  },
  {
    method: 'PUT',
    path: '/albums/{id}',
    handler: handler.putAlbum,
  },
  {
    method: 'DELETE',
    path: '/albums/{id}',
    handler: handler.deleteAlbum,
  },
];

module.exports = routes;
