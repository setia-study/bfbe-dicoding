class AlbumsHandler {
  constructor(service, validator) {
    this.albumService = service;
    this.validationService = validator;
    
    this.getAlbums = this.getAlbums.bind(this);
    this.getAlbumById = this.getAlbumById.bind(this);
    this.postAlbum = this.postAlbum.bind(this);
    this.putAlbum = this.putAlbum.bind(this);
    this.deleteAlbum = this.deleteAlbum.bind(this);
  }

  async getAlbums(req, res) {
    try {
      const result = await this.albumService.getAlbums();
      return res.response({
        status: 'success',
        data: { albums: result.rows ?? [] },
      }).code(200);
    } catch (error) {
      return error;
    }
  }

  async getAlbumById(req, res) {
    const { id } = req.params;
    try {
      const result = await this.albumService.getAlbumById(id);
      return res.response({
        status: 'success',
        data: { album: result },
      }).code(200);
    } catch (error) {
      return error;
    }
  }

  async postAlbum(req, res) {
    const data = req.payload;
    try {
      this.validationService.validation(data);
      const result = await this.albumService.addAlbum(data);
      return res.response({
        status: 'success',
        data: {
          albumId: result,
        },
      }).code(201);
    } catch (error) {
      return error;
    }
  }

  async putAlbum(req, res) {
    const { id } = req.params;
    const data = req.payload;
    try {
      this.validationService.validation(data);
      await this.albumService.updateAlbum(id, data);
      return res.response({
        status: 'success',
        message: 'Album updated!',
      }).code(200);
    } catch (error) {
      return error;
    }
  }

  async deleteAlbum(req, res) {
    const { id } = req.params;
    try {
      await this.albumService.deleteAlbum(id);
      return res.response({
        status: 'success',
        message: 'Album deleted!',
      }).code(200);
    } catch (error) {
      return error;
    }
  }
}

module.exports = AlbumsHandler;
