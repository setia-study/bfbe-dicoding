const ErrBadRequest = require('./errBadRequest');
const ErrNotFound = require('./errNotFound');

module.exports = { ErrBadRequest, ErrNotFound };
