const BaseError = require('./baseError');

class ErrBadRequest extends BaseError {
  constructor(message) {
    super(message, 400);
    this.message = message;
    this.name = 'Error Bad Request';
  }
}

module.exports = ErrBadRequest;
