const BaseError = require('./baseError');

class ErrNotFound extends BaseError {
  constructor(message) {
    super(message, 404);
    this.message = message;
    this.name = 'Not Found Error';
  }
}

module.exports = ErrNotFound;
