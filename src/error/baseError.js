class BaseError extends Error {
  message;
  code;

  constructor(message, code = 500) {
    super(message);
    this.name = 'Error Client';
    this.code = code;
  }
}

module.exports = BaseError;
