const { ErrBadRequest } = require('../../error');
const SongsSchemaPayload = require('./schema');

const SongsValidator = {
  validation: (data) => {
    const validate = SongsSchemaPayload.validate(data);
    if (validate.error) {
      throw new ErrBadRequest(validate.error.message);
    }
  },
};

module.exports = SongsValidator;
