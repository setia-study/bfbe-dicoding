const Joi = require('joi');

const currentYear = new Date().getFullYear();
const SongsSchemaPayload = Joi.object({
  albumId: Joi.string(),
  title: Joi.string().required(),
  year: (
    Joi.number()
      .integer()
      .min(1900)
      .max(currentYear)
      .required()
  ),
  genre: Joi.string().required(),
  performer: Joi.string().required(),
  duration: Joi.number(),
});

module.exports = SongsSchemaPayload;
