const { ErrBadRequest } = require('../../error');
const AlbumsSchemaPayload = require('./schema');

const AlbumsValidator = {
  validation: (data) => {
    const validate = AlbumsSchemaPayload.validate(data);
    if (validate.error) {
      throw new ErrBadRequest(validate.error.message);
    }
  },
};

module.exports = AlbumsValidator;
