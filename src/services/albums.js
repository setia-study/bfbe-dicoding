const { Pool } = require('pg');
const { nanoid } = require('nanoid');
const { ErrBadRequest, ErrNotFound } = require('../error');

class AlbumsService {
  constructor() {
    this.pool = new Pool();

    this.addAlbum = this.addAlbum.bind(this);
    this.getAlbums = this.getAlbums.bind(this);
    this.getAlbumById = this.getAlbumById.bind(this);
    this.updateAlbum = this.updateAlbum.bind(this);
    this.deleteAlbum = this.deleteAlbum.bind(this);
  }

  async addAlbum(album) {
    const id = `album-${nanoid(16)}`;

    const query = {
      text: 'INSERT INTO albums (id, name, year) VALUES ($1, $2, $3)',
      values: [id, album.name, album.year],
    };
    const result = await this.pool.query(query);
    if (result.rowCount === 0) {
      throw new ErrBadRequest('Album not added!');
    }
    return id;
  }

  async getAlbums() {
    const query = {
      text: 'SELECT * FROM albums',
    };
    const result = await this.pool.query(query);
    return result;
  }

  async getAlbumById(id) {
    const query = {
      text: `
      SELECT albums.id AS "albumId", albums.name, albums.year, songs.id, songs.title, songs.performer 
      FROM albums 
      LEFT JOIN songs ON songs."albumId" = albums.id
      WHERE albums.id = $1
      `,
      values: [id],
    };

    const result = await this.pool.query(query);
    if (result.rowCount === 0) {
      throw new ErrNotFound('Album not found!');
    }
    const album = {
      id: result.rows[0].albumId,
      name: result.rows[0].name,
      year: result.rows[0].year,
      songs: result.rows.map((song) => ({
        id: song.id,
        title: song.title,
        performer: song.performer,
      })),
    };
    return album;
  }

  async updateAlbum(id, album) {
    const query = {
      text: 'UPDATE albums SET name = $1, year = $2 WHERE id = $3',
      values: [album.name, album.year, id],
    };
    const result = await this.pool.query(query);
    if (result.rowCount === 0) {
      throw new ErrNotFound('Album not found!');
    }
    return result;
  }

  async deleteAlbum(id) {
    const query = {
      text: 'DELETE FROM albums WHERE id = $1',
      values: [id],
    };

    const result = await this.pool.query(query);
    if (result.rowCount === 0) {
      throw new ErrNotFound('Album not found!');
    }
    return result;
  }
}

module.exports = AlbumsService;
