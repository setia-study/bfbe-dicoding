const { Pool } = require('pg');
const { nanoid } = require('nanoid');
const { ErrBadRequest, ErrNotFound } = require('../error');

class SongsService {
  constructor() {
    this.pool = new Pool();

    this.addSong = this.addSong.bind(this);
    this.getSongs = this.getSongs.bind(this);
    this.getSongById = this.getSongById.bind(this);
    this.updateSong = this.updateSong.bind(this);
    this.deleteSong = this.deleteSong.bind(this);
  }

  async addSong(song) {
    const id = `song-${nanoid(16)}`;
    const query = {
      text: 'INSERT INTO songs (id, title, year, genre, performer, duration, "albumId") VALUES ($1, $2, $3, $4, $5, $6, $7)',
      values: [id, song.title, song.year, song.genre, song.performer, song.duration, song.albumId],
    };
    const result = await this.pool.query(query);
    if (result.rowCount === 0) {
      throw new ErrBadRequest('Song not added!');
    }
    return id;
  }

  async getSongs() {
    const query = {
      text: 'SELECT id, title, performer FROM songs',
    };
    const result = await this.pool.query(query);
    return result;
  }

  async getSongById(id) {
    const query = {
      text: 'SELECT * FROM songs WHERE id = $1',
      values: [id],
    };

    const result = await this.pool.query(query);
    if (result.rowCount === 0) {
      throw new ErrNotFound('Song not found!');
    }
    return result.rows[0];
  }

  async updateSong(id, song) {
    const query = {
      text: 'UPDATE songs SET title = $1, year = $2, genre = $3, performer = $4, duration = $5, "albumId" = $6 WHERE id = $7',
      values: [song.title, song.year, song.genre, song.performer, song.duration, song.albumId, id],
    };
    const result = await this.pool.query(query);
    if (result.rowCount === 0) {
      throw new ErrNotFound('Song not found!');
    }
  }

  async deleteSong(id) {
    const query = {
      text: 'DELETE FROM songs WHERE id = $1',
      values: [id],
    };

    const result = await this.pool.query(query);
    if (result.rowCount === 0) {
      throw new ErrNotFound('Song not found!');
    }
  }
}

module.exports = SongsService;
