const ResponseInterceptor = (req, h) => {
  const { response } = req;

  if (response instanceof Error) {
    const newResponse = h.response({
      status: 'fail',
      message: response.message,
    });
    newResponse.code(response.code || 500);
    return newResponse;
  }
  return h.continue;
};

module.exports = ResponseInterceptor;
