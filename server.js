const Hapi = require('@hapi/hapi');
const dotenv = require('dotenv');
const album = require('./src/api/albums/index');
const songs = require('./src/api/songs/index');
const ResponseInterceptor = require('./src/extensions/interceptor');
const AlbumsService = require('./src/services/albums');
const SongService = require('./src/services/songs');
const { AlbumsValidator, SongsValidator } = require('./src/validator/index');

const init = async () => {
  dotenv.config();
  const app = Hapi.server({
    host: process.env.HOST,
    port: process.env.PORT,
    routes: {
      cors: {
        origin: ['*'],
      },
    },
  });

  await app.register({
    plugin: album,
    options: {
      service: new AlbumsService(),
      validator: AlbumsValidator,
    },
  });
  await app.register({
    plugin: songs,
    options: {
      service: new SongService(),
      validator: SongsValidator,
    },
  });

  app.ext('onPreResponse', ResponseInterceptor);

  await app.start();
  // eslint-disable-next-line no-console
  console.log(`Running at ${app.info.uri}`);
};

init();
